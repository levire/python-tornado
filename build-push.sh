#!/bin/bash

docker build -f ./Dockerfile-minimal -t levire/python-tornado:latest -t levire/python-tornado:p3.6-t5.0.2 .
docker build -f ./Dockerfile-mongo -t levire/python-tornado-mongo:latest -t levire/python-tornado-mongo:p3.6-t5.0.2 .

docker push levire/python-tornado:latest
docker push levire/python-tornado:p3.6-t5.0.2

docker push levire/python-tornado-mongo:latest
docker push levire/python-tornado-mongo:p3.6-t5.0.2